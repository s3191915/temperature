package nl.utwente.di.temperature;

public class Converter {
    double getConvertedTemperature(double celsius) {
        return celsius * 9/5 + 32;
    }
}
