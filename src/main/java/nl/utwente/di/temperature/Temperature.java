package nl.utwente.di.temperature;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets a temperature in Celsius and converts it to Fahrenheit
 */

public class Temperature extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Converter converter;
	
    public void init() throws ServletException {
    	converter = new Converter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature";

    String result;
    try {
        double celsius = Double.parseDouble(request.getParameter("celsius"));
        double fahrenheit = converter.getConvertedTemperature(celsius);
        result = celsius + "C = " + fahrenheit + "F";
    } catch (NumberFormatException e) {
        result = "Not a number.";
    }
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>" + result +
                "</BODY></HTML>");
  }
}
