package nl.utwente.di.temperature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {

    @Test
    public void testBook1() throws Exception{
        Converter converter = new Converter();
        double celsius = converter.getConvertedTemperature(0);
        Assertions.assertEquals(32, celsius, 0.0, "0C to F");
    }
}
